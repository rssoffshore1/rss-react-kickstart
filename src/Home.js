import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUsers } from './actions/home';

class Home extends Component {
 constructor(props) {
   super(props);
   this.state = {
     usersLoaded: false
   };
   this._onLoadUsers = this._onLoadUsers.bind(this);
 }
 componentWillMount() {
  
 }
 componentWillReceiveProps(newProps) {
  if(newProps) {
    this.setState({
      usersLoaded: true
    });
  }
 }
 _renderUsers() {
   let { usersLoaded } = this.state;
   let { users } = this.props;
   if(usersLoaded) {
    let usersMarkup = users.map(item => <li key={item.id}>{item.name}({item.email})</li>)
     return (
        <ul>
          {usersMarkup}
        </ul>
     );
   }
 }

 _onLoadUsers() {
   this.props.dispatch(getUsers());
 }

 render() {
   let usersList = this._renderUsers();
   return (
     <div>
        <h1>Home</h1>
        <button onClick={this._onLoadUsers}>Load Users</button>
        {usersList}
      </div>
    );
  }
}

let select = (state) => ({
  users: state.home.users
});

export default connect(select) (Home);