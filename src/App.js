import React, { Component } from 'react';
import './assets/css/default.min.css';
import Header from './components/Header';
import TodosList from './components/TodosList';
import AddTodo from './components/AddTodo';
import { addTodo } from './actions/todos';
import { connect } from 'react-redux';

class App extends Component {
  constructor(props) {
    super(props);
    this._handleAddTodo = this._handleAddTodo.bind(this);
  }
  _handleAddTodo(text) {
    this.props.dispatch(addTodo(text));
  }  
  render() {
    return (
      <div className="App">
        <Header/>
        <TodosList/>
        <AddTodo onClick={this._handleAddTodo}>Add todo</AddTodo>
        {this.props.children}
      </div>
    );
  }
}

export default connect() (App);
