import { applyMiddleware, createStore, compose, combineReducers } from "redux";

import { createLogger } from 'redux-logger'
import thunk from "redux-thunk";
import { devTools } from 'redux-devtools';
import todos from "./reducers/todos";
import home from './reducers/home';

const middleware = applyMiddleware(thunk, createLogger());
 
export default createStore(combineReducers({ todos, home }),
compose(middleware, window.devToolsExtension ? window.devToolsExtension() : devTools()));