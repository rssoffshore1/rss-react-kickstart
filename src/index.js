import React from 'react';
import ReactDOM from 'react-dom';
import store from "./store";
import { Provider } from "react-redux";
import { Router, hashHistory } from 'react-router';
import routes from './Routes';

ReactDOM.render(
    <Provider store={store}>
      <Router history={hashHistory}>
        {routes}
      </Router>
    </Provider>
, document.getElementById('root'));

