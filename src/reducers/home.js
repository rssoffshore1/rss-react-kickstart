import { GET_USERS_INIT, GET_USERS_SUCCESS, GET_USERS_FAILED } from '../actions/Actions';
    
    export const initialState = {
        users: undefined,
        error: undefined
    };
    const handlers = {
      [GET_USERS_INIT]: (state, action) => ({
        ...state,
        users: undefined,
        error: undefined
      }),
      [GET_USERS_SUCCESS]: (state, action) => ({
        ...state,
        users: action.response,
        error: undefined
      }),
      [GET_USERS_FAILED]: (state, action) => ({
        ...state,
        users: undefined,
        error: action.error
      })
    };
    
    export default function homeReducer(state = initialState, action) {
      let handler = handlers[action.type];
      if(!handler) return state;
      return { ...state, ...handler(state, action)};
    }