import React, { Component } from 'react';
import { connect } from 'react-redux';
import Todo from './Todo';
import { toggleTodo } from '../actions/todos';

export class TodosList extends Component {
  _handleAddToggleTodo(id) {
    this.props.dispatch(toggleTodo(id));
  }
  render () {
    let { todos } = this.props;
    return (
        <ul>
          {todos.map(todo =>
            <Todo
              key={todo.id}
              todoText = {todo.text}
              completed = {todo.completed}
              onClick={this._handleAddToggleTodo.bind(this, todo.id)}
            />
          )}
        </ul>
    );
  }
}

let select = (state) => ({
  todos: state.todos.todos
});

export default connect(select) (TodosList);