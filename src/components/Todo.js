import React, { Component } from 'react';

export default class Todo extends Component {
  render() {
    let { todoText, completed } = this.props;
    return (
      <li onClick={this.props.onClick}  style={{
        textDecoration: completed ? 'line-through' : 'none'
      }}>
        {todoText}
      </li>
    );
  }
}
