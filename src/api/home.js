export function getUsersApi() {
     return fetch(`https://jsonplaceholder.typicode.com/users`).then(res => res.json());
}