
import React from 'react';
import App from './App';
import Home from './Home';
import About from './About';
import { Route } from 'react-router';

export default (
    <Route path="/" component={App}>
        <Route path="/home" component={Home}/>
        <Route path="/about" component={About}/>
    </Route>
);