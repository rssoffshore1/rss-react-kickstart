import { getUsersApi } from '../api/home';
export const GET_USERS_INIT = 'GET_USERS_INIT';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAILED = 'GET_USERS_FAILED';

export function getUsers() {
    return function (dispatch) {
      dispatch({ type: GET_USERS_INIT });
      getUsersApi()
        .then(response => dispatch({ type: GET_USERS_SUCCESS, response}))
        .catch(error => dispatch({ type: GET_USERS_FAILED, error }));
    };
  }